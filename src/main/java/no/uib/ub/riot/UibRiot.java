/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.uib.ub.riot;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import org.apache.jena.graph.Graph;
import org.apache.jena.query.Dataset;
import org.apache.jena.query.ReadWrite;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFFormat;
import org.apache.jena.riot.RDFWriterRegistry;
import org.apache.jena.tdb.TDBFactory;

/**
 *
 * @author ogj077
 */
public class UibRiot {

    /**
     * @param args the command line arguments
     *///-DdirPath -Doutput
    public static void main(String[] args) throws FileNotFoundException {
        
        String dirPath = System.getProperty("dirPath");
        String outputFile = System.getProperty("outFile");
        String named = System.getProperty("namedGraph");
        Dataset dataset = TDBFactory.createDataset(dirPath);
      
        dataset.begin(ReadWrite.READ);
        Model model = dataset.getNamedModel(named);
        OutputStream out  = new FileOutputStream(outputFile);                                                     
        RDFDataMgr.write(out, model, RDFFormat.RDFXML_PLAIN);
        dataset.end();
        dataset.close();
    }

}
